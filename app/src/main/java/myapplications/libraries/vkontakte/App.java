package myapplications.libraries.vkontakte;

import android.app.Application;

import com.vk.api.sdk.VK;
import com.vk.api.sdk.VKTokenExpiredHandler;
import com.vk.api.sdk.utils.VKUtils;


public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        VK.addTokenExpiredHandler(tokenTracker);
        String[] fingerprints = VKUtils.getCertificateFingerprint(this, this.getPackageName());
    }

    public VKTokenExpiredHandler getTokenTracker() {
        return tokenTracker;
    }

    VKTokenExpiredHandler tokenTracker = new VKTokenExpiredHandler() {
        @Override
        public void onTokenExpired() {

        }
    };


}