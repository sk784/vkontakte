package myapplications.libraries.vkontakte.mvp.model.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {
    @SerializedName ("id")
    @Expose
    public long id;

    @SerializedName ("first_name")
    @Expose
    public String firstName;

    @SerializedName ("second_name")
    @Expose
    public String secondName;

    @SerializedName ("bdate")
    @Expose
    public String birthDay;

    @SerializedName ("city")
    @Expose
    public UserDataCity city;

    @SerializedName ("photo_100")
    @Expose
    public String userPhoto;

    @SerializedName ("last_seen")
    @Expose
    public UserDataLastSeen lastSeen;

    @SerializedName ("university_name")
    @Expose
    public String education;

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public String getEducation() {
        return education;
    }

    public UserDataCity getCity() {
        return city;
    }

    public UserDataLastSeen getLastSeen() {
        return lastSeen;
    }
}
