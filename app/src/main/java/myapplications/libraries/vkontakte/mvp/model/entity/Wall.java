package myapplications.libraries.vkontakte.mvp.model.entity;

public class Wall {

    private User user;
    private Long id;
    private String creationDate;
    private String text;
    private String imageUrl;
    private Long viewsCount;
    private Long favouriteCount;

    public Wall(User user, Long id, String creationDate, String text, String imageUrl, Long viewsCount, Long favouriteCount) {
        this.user = user;
        this.id = id;
        this.creationDate = creationDate;
        this.text = text;
        this.imageUrl = imageUrl;
        this.viewsCount = viewsCount;
        this.favouriteCount = favouriteCount;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getText() {
        return text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Long getViewsCount() {
        return viewsCount;
    }

    public Long getFavouriteCount() {
        return favouriteCount;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setViewsCount(Long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public void setFavouriteCount(Long favouriteCount) {
        this.favouriteCount = favouriteCount;
    }
}
