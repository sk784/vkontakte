package myapplications.libraries.vkontakte.mvp.model.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class UserDataLastSeen {


    @SerializedName("time")
    @Expose
    public String lastSeenTime;

    public String getLastSeenTime() {
        return lastSeenTime;
    }
}
