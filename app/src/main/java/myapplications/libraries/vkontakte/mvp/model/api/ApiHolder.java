package myapplications.libraries.vkontakte.mvp.model.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHolder {
    private static ApiHolder instance = new ApiHolder();
    public static ApiHolder getInstance() {
        if(instance == null){
            instance = new ApiHolder();
        }
        return instance;
    }

    private IDataSource api;

    private ApiHolder() {
        Gson gson = new GsonBuilder().create();
        api = new Retrofit.Builder()
                .baseUrl("https://api.vk.com/method/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(IDataSource.class);
    }



    public static IDataSource getApi() {
        return instance.api;
    }
}
