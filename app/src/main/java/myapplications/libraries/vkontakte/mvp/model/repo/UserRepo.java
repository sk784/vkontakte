package myapplications.libraries.vkontakte.mvp.model.repo;



import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import myapplications.libraries.vkontakte.mvp.model.api.ApiHolder;
import myapplications.libraries.vkontakte.mvp.model.api.model.UserData;




public class UserRepo {


    private String keyApi = "73d256add957a07c610fabd2588fb931fc81b2817856f0306362824fda1ba57441048b1779db49ddfdb29";
    private String apiVersion = "5.102";


    public Single<UserData> getUser() {

        return ApiHolder.getApi().getUserData("userId","photo_200","bdate","last_seen","city","education",keyApi,apiVersion)
                .subscribeOn(Schedulers.io());
    }
}
