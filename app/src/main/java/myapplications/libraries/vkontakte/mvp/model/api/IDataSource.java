package myapplications.libraries.vkontakte.mvp.model.api;

import io.reactivex.Single;
        import myapplications.libraries.vkontakte.mvp.model.api.model.UserData;
        import retrofit2.http.GET;
        import retrofit2.http.Query;

public interface IDataSource {
    @GET("users.get")
    Single<UserData> getUserData(@Query("user_ids")String userId,
                                 @Query("fields") String userPhoto,
                                 @Query("fields") String bdate,
                                 @Query("fields") String last_seen,
                                 @Query("fields") String city,
                                 @Query("fields") String education,
                                 @Query("access_token") String keyApi,
                                 @Query("v") String apiVersion);
}
