package myapplications.libraries.vkontakte.mvp.presenter;

import android.annotation.SuppressLint;


import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.Scheduler;

import myapplications.libraries.vkontakte.mvp.model.repo.UserRepo;
import myapplications.libraries.vkontakte.mvp.view.MainView;


public class MainPresenter extends MvpPresenter<MainView> {

    private UserRepo userRepo;
    private Scheduler mainThreadScheduler;

    public MainPresenter(Scheduler mainThreadScheduler) {
        this.userRepo = new UserRepo();
        this.mainThreadScheduler = mainThreadScheduler;
    }


    @SuppressLint("CheckResult")
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().init();
        loadUser();
    }


    @SuppressLint("CheckResult")
    private void loadUser() {
        getViewState().showLoading();
        userRepo.getUser()
                .observeOn(mainThreadScheduler)
                .subscribe(userData -> {
                    getViewState().hideLoading();
                    getViewState().setUserName(userData.getFirstName()+" "+ userData.getSecondName());
                    //getViewState().setUserLastSeen(userData.getLastSeen().get);
                    //getViewState().setUserCity(userData.getCity().get);
                    getViewState().setUserBirthday(userData.getBirthDay());
                    getViewState().setUserEducation(userData.getEducation());
                    getViewState().loadImage(userData.getUserPhoto());
                }, t -> getViewState().hideLoading());
    }
}
