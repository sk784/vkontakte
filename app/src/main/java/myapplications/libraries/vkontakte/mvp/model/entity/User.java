package myapplications.libraries.vkontakte.mvp.model.entity;

public class User {

    private long id;
    private String imageUrl;
    private String name;
    private String lastSeen;
    private String city;
    private String birthday;
    private String education;

    public User(long id, String imageUrl, String name, String lastSeen, String city, String birthday, String education) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.name = name;
        this.lastSeen = lastSeen;
        this.city = city;
        this.birthday = birthday;
        this.education = education;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public String getCity() {
        return city;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEducation() {
        return education;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setEducation(String education) {
        this.education = education;
    }


}
