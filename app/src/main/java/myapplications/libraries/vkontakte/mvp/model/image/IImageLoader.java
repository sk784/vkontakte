package myapplications.libraries.vkontakte.mvp.model.image;

public interface IImageLoader<T> {
    void loadInto(String url, T container);
}
