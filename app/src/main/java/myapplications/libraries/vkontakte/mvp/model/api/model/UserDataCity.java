package myapplications.libraries.vkontakte.mvp.model.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class UserDataCity {

    @SerializedName("title")
    @Expose
    public String cityTitle;

    public String getCityTitle() {
        return cityTitle;
    }
}
