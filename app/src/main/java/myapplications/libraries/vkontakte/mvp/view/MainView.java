package myapplications.libraries.vkontakte.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(value = AddToEndSingleStrategy.class)
public interface MainView extends MvpView {
    void init();
    void updateList();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showLoading();
    void hideLoading();

    void setUserName(String name);
    void setUserLastSeen(String lastSeen);
    void setUserCity(String city);
    void setUserBirthday(String birthday);
    void setUserEducation(String education);

    void loadImage(String userImage);
}