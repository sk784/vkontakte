package myapplications.libraries.vkontakte.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.vk.api.sdk.utils.VKUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import myapplications.libraries.vkontakte.R;

import myapplications.libraries.vkontakte.mvp.model.image.IImageLoader;
import myapplications.libraries.vkontakte.mvp.presenter.MainPresenter;
import myapplications.libraries.vkontakte.mvp.view.MainView;
import myapplications.libraries.vkontakte.ui.image.PicassoImageLoader;
//import myapplications.libraries.vkontakte.ui.adapter.WallAdapter;


public class UserInfoActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.user_image_view)
    ImageView userImageView;

    @BindView(R.id.user_name_text_view)
    TextView nameTextView;

    @BindView(R.id.user_last_seen_text_view)
    TextView userLastSeenTextView;

    @BindView(R.id.user_city_text_view)
    TextView userCityTextView;

    @BindView(R.id.user_bdate_text_view)
    TextView userBirthdayTextView;

    @BindView(R.id.user_education_text_view)
    TextView userEducationTextView;

    @BindView(R.id.rl_loading)
    RelativeLayout loadingRelativeLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.wall_recycler_view)
    RecyclerView wallRecyclerView;


//    private WallAdapter wallAdapter;

    IImageLoader<ImageView> imageLoader = new PicassoImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    @ProvidePresenter
    public MainPresenter providePresenter(){
        return new MainPresenter(AndroidSchedulers.mainThread());
    }

    public static void startFrom(Context context){
        Intent intent = new Intent(context, UserInfoActivity.class);
        context.startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.user_info_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == R.id.action_search) {
//            Intent intent = new Intent(this, SearchUsersActivity.class);
//            startActivity(intent);
//        }
//        return true;
//    }


    @Override
    public void init() {
        ViewCompat.setNestedScrollingEnabled(wallRecyclerView, false);
        wallRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        wallRecyclerView.setLayoutManager(new LinearLayoutManager(this));
     //   wallAdapter = new WallAdapter();
     //   wallRecyclerView.setAdapter(wallAdapter);
    }

    @Override
    public void updateList() {
     //   wallAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        loadingRelativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingRelativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void setUserName(String name) {
        nameTextView.setText(name);
    }

    @Override
    public void setUserLastSeen(String lastSeen) {
        userLastSeenTextView.setText(lastSeen);
    }

    @Override
    public void setUserCity(String city) {
       // userCityTextView.setText(city);
    }

    @Override
    public void setUserBirthday(String birthday) {
        userBirthdayTextView.setText(birthday);
    }

    @Override
    public void setUserEducation(String education) {
        userEducationTextView.setText(education);
    }

    @Override
    public void loadImage(String userImage) {
        imageLoader.loadInto(userImage, userImageView);
    }
}
