//package myapplications.libraries.vkontakte.ui.adapter;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.squareup.picasso.Picasso;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import myapplications.libraries.vkontakte.R;
//import myapplications.libraries.vkontakte.mvp.model.entity.Wall;
//
//public class WallAdapter extends RecyclerView.Adapter<WallAdapter.WallViewHolder> {
//    private static final String VKONTAKTE_RESPONSE_FORMAT="EEE MMM dd HH:mm:ss ZZZZZ yyyy"; // Thu Oct 26 07:31:08 +0000 2017
//    private static final String MONTH_DAY_FORMAT = "MMM d"; // Oct 26
//
//    private List<Wall> wallList = new ArrayList<>();
//
//    @NotNull
//    @Override
//    public WallViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.wall_item_view, parent, false);
//        return new WallViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NotNull WallViewHolder holder, int position) {
//        holder.bind(wallList.get(position));
//    }
//
//    @Override
//    public int getItemCount() {
//        return wallList.size();
//    }
//
//    public void setItems(Collection<Wall> tweets) {
//        wallList.addAll(tweets);
//        notifyDataSetChanged();
//    }
//
//    public void clearItems() {
//        wallList.clear();
//        notifyDataSetChanged();
//    }
//
//    class WallViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.profile_image_view)
//        ImageView userImageView;
//
//        @BindView(R.id.author_name_text_view)
//        TextView nameTextView;
//
//        @BindView(R.id.creation_date_text_view)
//        TextView creationDateTextView;
//
//        @BindView(R.id.wall_content_text_view)
//        TextView contentTextView;
//
//        @BindView(R.id.wall_image_view)
//        ImageView wallImageView;
//
//        @BindView(R.id.likes_text_view)
//        TextView likesTextView;
//
//        @BindView(R.id.views_text_view)
//        TextView viewsTextView;
//
//        public WallViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//        }
//
//        public void bind(Wall wall) {
//            nameTextView.setText(wall.getUser().getName());
//            contentTextView.setText(wall.getText());
//            viewsTextView.setText(String.valueOf(wall.getViewsCount()));
//            likesTextView.setText(String.valueOf(wall.getFavouriteCount()));
//
//            String creationDateFormatted = getFormattedDate(wall.getCreationDate());
//            creationDateTextView.setText(creationDateFormatted);
//
//            Picasso.get().load(wall.getUser().getImageUrl()).into(userImageView);
//
//            String wallPhotoUrl = wall.getImageUrl();
//            Picasso.get().load(wallPhotoUrl).into(wallImageView);
//
//            wallImageView.setVisibility(wallPhotoUrl != null ? View.VISIBLE : View.GONE);
//        }
//
//        private String getFormattedDate(String rawDate) {
////            SimpleDateFormat utcFormat = new SimpleDateFormat(VKONTAKTE_RESPONSE_FORMAT, Locale.ROOT);
////            SimpleDateFormat displayedFormat = new SimpleDateFormat(MONTH_DAY_FORMAT, Locale.getDefault());
////            try {
////                Date date = utcFormat.parse(rawDate);
////                return displayedFormat.format(date);
////            } catch (ParseException e) {
////                throw new RuntimeException(e);
////            }
////        }
////    }
//}