package myapplications.libraries.vkontakte.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.widget.Button;


import com.vk.api.sdk.VK;

import com.vk.api.sdk.auth.VKAccessToken;
import com.vk.api.sdk.auth.VKAuthCallback;
import com.vk.api.sdk.auth.VKScope;
import com.vk.api.sdk.utils.VKUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import myapplications.libraries.vkontakte.R;


public class AuthActivity extends AppCompatActivity {

    private ArrayList<VKScope> scope = new ArrayList<>();

    @BindView(R.id.loginBtn)
    Button vkLoginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (VK.isLoggedIn()) {
            UserInfoActivity.startFrom(this);
            finish();
            return;
        }
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        scope.add(VKScope.PHOTOS);
        scope.add(VKScope.WALL);

        vkLoginButton.setOnClickListener(v -> {
            VK.login(this, scope);
        });
    }


    public static void startFrom(Context context){
        Intent intent = new Intent(context, AuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKAuthCallback callback = new VKAuthCallback() {
            @Override
            public void onLoginFailed(int i) {

            }

            public void onLogin(@NotNull VKAccessToken token) {
                UserInfoActivity.startFrom(AuthActivity.this);
                finish();
            }
        };

        if (!VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        }
    }

