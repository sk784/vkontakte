package myapplications.libraries.vkontakte.ui.image;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import myapplications.libraries.vkontakte.mvp.model.image.IImageLoader;

public class PicassoImageLoader implements IImageLoader<ImageView> {
    @Override
    public void loadInto(String url, ImageView container) {
        Picasso.get().load(url).into(container);
    }
}